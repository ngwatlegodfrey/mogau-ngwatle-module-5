import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mogau_ngwatle_module_5/main.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final CollectionReference _contacts =
      FirebaseFirestore.instance.collection("contacts");

  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  Future<void> _update([DocumentSnapshot? documentSnapshot]) async {
    if (documentSnapshot != null) {
      _firstnameController.text = documentSnapshot['firstname'];
      _lastnameController.text = documentSnapshot['lastname'];
      _phoneController.text = documentSnapshot['phone'].toString();
    }

    await showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
              top: 20,
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom + 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextField(
                controller: _firstnameController,
                decoration: const InputDecoration(labelText: "Firtsname"),
              ),
              TextField(
                controller: _lastnameController,
                decoration: const InputDecoration(labelText: "Lastsname"),
              ),
              TextField(
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                controller: _phoneController,
                decoration: const InputDecoration(labelText: "Phone"),
              ),
              const SizedBox(height: 20.0),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Color(0xffdc143c),
                  ),
                  child: const Text("Update"),
                  onPressed: () async {
                    final String firstname = _firstnameController.text;
                    final String lastname = _lastnameController.text;
                    final int? phone = int.tryParse(_phoneController.text);
                    if (phone != null) {
                      await _contacts.doc(documentSnapshot!.id).update({
                        "firstname": firstname,
                        "lastname": lastname,
                        "phone": phone
                      });
                      _firstnameController.text = "";
                      _lastnameController.text = "";
                      _phoneController.text = "";
                    }
                  }),
            ],
          ),
        );
      },
    );
  }

  Future<void> _create([DocumentSnapshot? documentSnapshot]) async {
    if (documentSnapshot != null) {
      _firstnameController.text = documentSnapshot['firstname'];
      _lastnameController.text = documentSnapshot['lastname'];
      _phoneController.text = documentSnapshot['phone'].toString();
    }

    await showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: EdgeInsets.only(
              top: 20,
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom + 20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextField(
                controller: _firstnameController,
                decoration: const InputDecoration(labelText: "Firtsname"),
              ),
              TextField(
                controller: _lastnameController,
                decoration: const InputDecoration(labelText: "Lastsname"),
              ),
              TextField(
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                controller: _phoneController,
                decoration: const InputDecoration(labelText: "Phone"),
              ),
              const SizedBox(height: 20.0),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Color(0xffdc143c),
                  ),
                  child: const Text("Create"),
                  onPressed: () async {
                    final String firstname = _firstnameController.text;
                    final String lastname = _lastnameController.text;
                    final int? phone = int.tryParse(_phoneController.text);
                    if (phone != null) {
                      await _contacts.add({
                        "firstname": firstname,
                        "lastname": lastname,
                        "phone": phone
                      });
                      _firstnameController.text = "";
                      _lastnameController.text = "";
                      _phoneController.text = "";
                    }
                  }),
            ],
          ),
        );
      },
    );
  }

  Future<void> _delete(String contactId) async {
    await _contacts.doc(contactId).delete();
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text("You have successfully deleted a contact"),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("LIST OF CONTACTS"),
        centerTitle: true,
        backgroundColor: Color(0xffdc143c),
      ),
      body: StreamBuilder(
        stream: _contacts.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
                itemCount: streamSnapshot.data!.docs.length,
                itemBuilder: (context, index) {
                  final DocumentSnapshot documentSnapshot =
                      streamSnapshot.data!.docs[index];
                  return Card(
                    clipBehavior: Clip.antiAlias,
                    margin: const EdgeInsets.all(10),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Fullname: " + documentSnapshot['firstname'],
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(width: 8.0),
                              Text(
                                documentSnapshot['lastname'],
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Phone: +27" +
                                    documentSnapshot['phone'].toString(),
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              IconButton(
                                color: Color(0xffdc143c),
                                icon: Icon(Icons.edit),
                                onPressed: () {
                                  _update(documentSnapshot);
                                },
                              ),
                              SizedBox(width: 8.0),
                              IconButton(
                                color: Color(0xffdc143c),
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  _delete(documentSnapshot.id);
                                },
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                });
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _create(),
        backgroundColor: Color(0xffdc143c),
        child: const Icon(Icons.add),
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
